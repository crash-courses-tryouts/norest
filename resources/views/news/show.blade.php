@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{route('news.index')}}"><button class="btn btn-primary"><i class="fas fa-chevron-left"></i>&nbsp; Voltar Atrás</button></a>
<h2>{{$article->title}}</h2>
<small>Publicado a {{$article->created_at->format('Y-m-d H:i:s')}} por {{$article->user->name}}</small><br>
<h5 style="display:inline">Estado:</h5> <h6 style="display:inline; @if($article->deleted_at!=NULL) text-decoration: line-through"@else" @endif>{{$article->state->description}}@if($article->deleted_at!=NULL)<h5 class="alert-danger">ARTIGO APAGADO</h5>@endif</h6>
<h2 class="mt-2">Conteudo:</h2>
<div class="container  mt-3">
<div class="font-weight-light">{!!$article->content!!}</div>
</div>
@endsection