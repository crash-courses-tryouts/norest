@extends('layouts.app')
@section('content')
    
<div class="container">
    <h2>Criar Artigo</h2>
        <a href="{{route('news.index')}}"><button class="btn btn-primary"><i class="fas fa-chevron-left"></i>&nbsp; Voltar Atrás</button></a>
<form method="POST" id="formcreate" action="{{route('news.store')}}">
    @csrf
    <label for="title">Titulo:</label>
    <input type="text" class="form-control" name="title"><br>
    <label for="content">Conteudo: </label>
    <textarea class="form-control" name="content"></textarea>
    <label>Estado:</label>
    <select class="form-control" name="state">
    @if(count($states))
    @foreach($states as $state)
    <option value="{{$state->id}}">{{$state->description}}</option>
    @endforeach
    @else
     <option value="1">Publicado</option>
     <option value="2">Não Publicado</option>
    @endif
    </select><br>
    <button class="btn btn-primary" type="submit">Adicionar Noticia</button>
</form>
</div>
@endsection