@if (count($errors)>0)
    @foreach($errors->all() as $error) <!--variavel errors que retorna ao falhar validacao -->
        <div class="container">
            <p>
                <div class="alert alert-warning">
                    <i class="fas fa-exclamation">&nbsp;</i>
                    {{$error}}
                </div>
            </p>
        </div>
    @endforeach
@endif



@if (Session::has('flash-success'))
<p>
        <div class="alert alert-success">
            <i class="fas fa-check">&nbsp;</i>
            {!!session('flash-success')!!}
        </div>
    </p>
@endif

@if (Session::has('flash-error'))
    <p>
        <div class="alert alert-danger">
            <i class="fas fa-exclamation-triangle">&nbsp;</i>
             {{session('error')}}
        </div>
    </p>
@endif