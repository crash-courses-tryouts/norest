@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header">{{$news->title}}
                <div style="float:right">
                    @if ($news->title!="Artigos")<!--Titulo Pagina Principal -->
                    <a href="{{route('news.index')}}" ><button class="btn btn-primary"><i class="fas fa-home"></i>Voltar à Home</button></a>   
                    @else
                    <a href="{{route('news.create')}}"><button class="btn btn-success"><i class="fas fa-plus"></i> Criar Artigo</button></a>
                    <a href="{{route('news.deleted')}}" ><button class="btn btn-danger"><i class="fas fa-trash"></i> Artigos Apagados</button></a>
                    @endif
                </div>
            </div>
                <div class="card-body">
                  
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4" style="border-right:0.5px solid grey">
                            <form method="get" action="{{route('news.search','q')}}">
                                <label for="qtitle">Titulo:</label>
                                <input type="title" class="form-control mb-4" name="qtitle" @if(isset($_GET) && isset($_GET['qtitle'])) value="{{$_GET['qtitle']}}"@endif >
                                <label for="qdate">Data de Publicacao:</label>
                                <input type="date" class="form-control mb-4" id="qdate" name="qdate" @if(isset($_GET) && isset($_GET['qdate'])) value="{{$_GET['qdate']}}"@endif>
                               <label for="qstate">Estado: </label>
                                <select class="form-control mb-3" name="qstate">
                                    <option @if(!isset($_GET['qstate'])) selected="true" @endif hidden disabled>Escolha um Estado</option>
                                    @if(count($states))
                                        @foreach($states as $state)
                                            <option value="{{$state->id}}" @if(isset($_GET) && isset($_GET['qstate']) && $_GET['qstate']==$state->id) selected @endif >{{$state->description}}</option>
                                        @endforeach
                                    @else
                                        <option value="1" @if(isset($_GET) && isset($_GET['qstate']) && $_GET['qstate']==1) selected @endif >Publicadoo</option>
                                        <option value="2" @if(isset($_GET) && isset($_GET['qstate']) && $_GET['qstate']==2) selected @endif >Não Publicado</option>
                                    @endif
                                </select>
                                {{-- <input type="hidden" name="trashed"  value=@if($news->title=="Artigos Apagados")1 @else 0 @endif> --}}
                                <label for="trashed">Area de Busca:</label>
                                <select class="form-control mb-3" name="trashed">
                                    <option value="0" @if(isset($_GET) && isset($_GET['trashed']) && $_GET['trashed']==0) selected @endif >Notícias Ativas</option>
                                    <option value="1" @if(isset($_GET) && isset($_GET['trashed']) && $_GET['trashed']==1) selected @endif>Notícias Apagadas</option>
                                    <option value="2" @if(isset($_GET) && isset($_GET['trashed']) && $_GET['trashed']==2) selected @endif>Ambos</option>
                                </select>

                                <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i>&nbsp; Pesquisar</button>
                            </form>
                            </div>
                            <div class="col-md-8">
                                
                                @if(isset($news) && count($news))
                                <table class="table">
                                    <tr><td>Titulo</td><td>Criador</td><td>Data Publicação</td><td>Estado</td><td class="text-center" colspan="2">Operacoes</td></tr>
                                
                                @foreach ($news as $article)
                                    <tr>
                                        <td>
                                                <a href="{{route('news.show',$article->id)}}">{{$article->title}}</a></td>
                                                <td>{{$article->user->name}}</td> 
                                                <td>{{$article->created_at->format('Y-m-d H:i:s')}}</td>
                                                
                                                <td @if ($article->deleted_at!=null) style="text-decoration: line-through"@endif>
                                                    {{$article->state->description}}
                                                </td> 
                                                @if($article->deleted_at!=null)
                                                <td>
                                                <a href="{{route('news.restore',$article->id)}}"><button class="btn btn-primary"><i class="fas fa-recycle"></i> &nbsp;Restaurar</button></a>
                                                </td>
                                                @else
                                                <td>
                                                    <a href="{{route('news.edit',$article->id)}}"><button class="btn btn-warning"><i class="fas fa-pencil-ruler"></i> &nbsp;Editar</button></a>
                                                </td>
                                                
                                                <td>
                                                  <form method="POST" action="{{route('news.destroy',$article->id)}}">  
                                                    @csrf
                                                    {{method_field('DELETE')}}
                                                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i>&nbsp;Apagar</button>
                                                  </form>
                                                
                                                </td>
                                                @endif
                                        </td>
                                    </tr>

                                    @endforeach
                                </table>
								
								
                                <p>{{ $news->appends(\Request::except('_token'))->render() }}</p>
                                
                                        </div>
                                        
                                    </div>
                                   
                                </div>
                              
                            
                                @else 
                                <h3>Não tem Noticias no Website!</h3>
                                @endif



                            </div>
                        </div>
                    </div>
                    
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script>
d=new Date();
var data= document.getElementById('qdate');
data.max=d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
</script>
@endsection
