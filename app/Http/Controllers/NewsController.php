<?php

namespace App\Http\Controllers;
use App\States;
use App\User;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news=News::GetAllNews();
        $states=States::All();
        $news->title="Artigos";
       return view('home',compact('news'),compact('states'));
    }

    
    public function deleted(){
        $news=News::GetAllNews(1);
        $states=States::All();
        $news->title="Artigos Apagados";
        return view('home',compact('news'),compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $states=States::All();
        return view('news.create',compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request,[
            'title'=>'required|min:3',
            'content'=>'required|max:200',
            'state'=>'required',
        ]);

        $data=[
            "id"=>NULL,
            "title"=>$request->input('title'),
            "content"=>$request->input('content'),
            "id_user"=>auth()->user()->id,
            "id_state"=>$request->input('state'),
            "created_at" =>  \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ];

        if(News::storeArticle($data))
            return redirect('/news')->with('flash-success','Noticia Criada com Sucesso');
        else
            return redirect('/news')->with('flash-error','Erro ao criar Noticia');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article=News::getArticle($id);
        //$article=News::findOrFail($id);
        return view('news.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $states=States::All();
        $article=News::getArticle($id);
        // $article=News::select("news.id","title","content","id_state")
        // ->where('news.id',$id)->first();
        return view('news.edit',compact('article'),compact('states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required|min:3',
            'content'=>'required|max:200',
            'state'=>'required',
        ]);

        $data=[
            "title"=>$request->input('title'),
            "content"=>$request->input('content'),
            "id_state"=>$request->input('state')
        ];

        if(News::storeArticle($data,$id))
            return redirect('/news')->with('flash-success','Noticia Atualizada com Sucesso');
        else
            return redirect('/news')->with('flash-error','Erro ao Atualizar Noticia');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(News::destroyArticle($id))
            return redirect('/news')->with('flash-success','Noticia Apagada com Sucesso');
        else
            return redirect('/news')->with('flash-error','Erro ao tentar apagar Noticia');
    }

    public function restore($id){
        
        if(News::restoreArticle($id))
            return redirect('/news')->with('flash-success','Noticia Restaurada com Sucesso');
        else
            return redirect('/news')->with('flash-error','Erro ao tentar restaurar Noticia');
    }

       
    public function search(Request $request)
    {
        $this->validate($request,['trashed'=>'required']);

        $data=[
            'title'=>$request->input('qtitle'),
            'created_at'=>$request->input('qdate'),
            'id_state'=>$request->input('qstate'),
            'trashed'=>$request->input('trashed')
        ];

        $search=[
            'qtitle'=>$request->input('qtitle'),
            'qdate'=>$request->input('qdate'),
            'qstate'=>$request->input('qstate'),
            'trashed'=>$request->input('trashed')
        ];

        $news=News::searchNews($data);
        $states=States::All();
        $news->title="Resultados Da Pesquisa";
        return view('home',compact('news'),compact('states'),compact('data'));
    }  
}