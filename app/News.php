<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table="news";
    public $primaryKey="id";
    public $timestamps=true;

    public function user(){
        return $this->hasOne('App\User','id','id_user');
    }

    public function state(){
        return $this->hasOne('App\States','id','id_state');
    }

    public function scopeGetAllNews($query,int $trashed=null):Object{
        if($trashed){ 
             $query->onlyTrashed();
        }
            return $query->orderBy('created_at','desc')->paginate(5);
        

        /*
        $query->select("news.id","title","name as publisher","description as state","news.created_at","news.deleted_at")
        ->join('users','id_user','users.id')
        ->join('states','id_state','states.id');
        if($trashed){
            $query=$query->onlyTrashed();
        }
        $query=$query->orderBy('created_at','desc')->paginate(5);
        return $query;*/
    }

    public function scopestoreArticle($query,array $data,int $id=null):bool{
        if($id){
            $query->where('news.id',$id);
            return  $query->update($data);
        }
        else{
            return $query->insert($data);
        }      
    }
    
    public function scopegetArticle($query,int $id):Object{
        return $query->withTrashed()->where('news.id',$id)->first();

    //   return $query->select("news.id","title","content","name as publisher","description as state","id_state","news.created_at","news.deleted_at")
    //     ->join('users','id_user','users.id')
    //     ->join('states','id_state','states.id')
    //     ->orderBy('created_at','desc')
    //     ->withTrashed() #para que seja possivel visualizar os documentos
    //     ->where('news.id',$id)->first();
        
    }

    public function scopedestroyArticle($query,int $id):bool{
       return $query->where('news.id',$id)->delete();
        //($query->delete())?1:0;
    }


    public function scoperestoreArticle($query,int $id):bool{
        return $query->onlyTrashed()->where('news.id',$id)->restore();
    }

    public function scopesearchNews($query,array $data):Object{       
        $order="DESC";

        // $query->select("news.id","title","name as publisher","description as state","news.created_at","news.deleted_at")
        // ->join('users','id_user','users.id')
        // ->join('states','id_state','states.id');

        if($data['id_state']){
            $query=$query->where('id_state',$data['id_state']);
        }
        if($data['created_at']){
            $query=$query->whereDate('news.created_at', '=',$data['created_at']); # >= para incluir artigos a partir de X dia
            $order="ASC";
        }

        if($data['title']){
            $query=$query->where('title','like','%'.$data['title'].'%');
        }

        if($data['trashed']==1){
            $query=$query->onlyTrashed();
        }
        elseif($data['trashed']==2){
            $query=$query->withTrashed();
        }

        $query=$query->orderBy('created_at',$order);
        $query=$query->paginate(5);
        return $query;
        }
    }